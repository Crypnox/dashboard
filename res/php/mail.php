<?php

$errors = [];
$errorMessage = '';
$success = false;

if (!empty($_POST)) {
    $name = $_POST['name'];
    $company = $_POST['company'];
    $email = $_POST['email'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    if (empty($name)) {
        $errors[] = 'Name ist leer';
    }

    if (empty($email)) {
        $errors[] = 'Email ist leer';
    } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $errors[] = 'Email ist ungültig';
    }

    if (empty($subject)) {
        $errors[] = 'Betreff ist leer';
    }

    if (empty($message)) {
        $errors[] = 'Nachricht ist leer';
    }

    if (empty($errors)) {
        $toEmail = 'marco.meissner@xn--drrenried-q9a.de';

        $headers = ['From' => "marco.meissner@xn--drrenried-q9a.de", 'Reply-To' => $email, 'Content-type' => 'text/html; charset=UTF-8'];

        $message = str_replace("\n.", "\n..", $message);

        $body = join(PHP_EOL, ["Name: {$name}", "Mail: {$email}", $message]);

        if (mail($toEmail, $subject, $body, $headers)) {
            $success = true;
            header("refresh:7;url=https://xn--drrenried-q9a.de/");
        } else {
            $errorMessage = 'Ihre Daten wurden richtig eingegeben, aber der Versand der Nachricht war fehlerhaft.';
        }
    } else {
        $allErrors = join('<br>', $errors);
        $errorMessage = "<p style='color: red;'>{$allErrors}</p>";
    }
}

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel='icon' href='images/favicon.png' type='image/' sizes="16x16"/>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/style.css">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

    <title>Marco Meißner</title>
</head>
<body>
<?php
if ($success) {
    echo '<div class="alert alert-success">Die Nachricht wurde <strong>Erfolgreich!</strong> versendet. Sie werden in 10 Sekunden automatisch wieder auf die Startseite weitergeleitet!</div>';
}

if (!empty($errorMessage)) {
    echo '<div class="alert alert-danger">
              <strong>Fehler!</strong> beim Senden der Nachricht. Bitte versuchen Sie es später erneut oder schreiben Sie eine Email direkt an <a href="mailto:marco.meissner@xn--drrenried-q9a.de?subject=Kontaktanfrage" target="_self">marco.meissner@dürrenried.de</a>!
            </div>
            <br>';
    $errorMessage;
}
?>
</body>
</html>