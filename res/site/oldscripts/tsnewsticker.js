function loadTSnewsticker() {
    var loading = "<div class=\"spinner-border text-warning\" style=\"width: 4rem; height: 4rem;\" role=\"status\"></div>";
    document.getElementById("tsnewsticker").innerHTML = loading;

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            parseXML(this);
        }
    };
    xmlhttp.open("GET", "https://www.tagesschau.de/newsticker.rdf", true);
    xmlhttp.send();
}