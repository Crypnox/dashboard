function loadHeise() {
  var loading = "<div class=\"spinner-border text-warning\" style=\"width: 4rem; height: 4rem;\" role=\"status\"></div>";
  document.getElementById("npc").innerHTML = loading;

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      parseHeiseXML(this, "heise");
    }
  };
  xmlhttp.open("GET", "https://www.heise.de/rss/heise.rdf", true);
  xmlhttp.send();
}

