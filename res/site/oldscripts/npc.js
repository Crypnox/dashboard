function loadNPC() {
  var loading = "<div class=\"spinner-border text-warning\" style=\"width: 4rem; height: 4rem;\" role=\"status\"></div>";
  document.getElementById("npc").innerHTML = loading;

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      parseXML(this);
    }
  };
  xmlhttp.open("GET", "https://www.np-coburg.de/auf-einen-blick.rss2.feed", true);
  xmlhttp.send();
}


/*
function parseXML(xml) {
  var itemXML = xml.responseXML.getElementsByTagName("item");
//console.log(itemXML);

  var news = [];
  var title = "";
  var link = "";
  var description = "";
  var carousel = "";

  for (var i = 0; i < itemXML.length; i++) {

      link = itemXML[i].getElementsByTagName("link")[0].childNodes[0].nodeValue.toString();
      description = itemXML[i].getElementsByTagName("description")[0].childNodes[0].nodeValue.toString();
      title = itemXML[i].getElementsByTagName("title")[0].childNodes[0].nodeValue.toString();

      news[i] = getHtml(title, description, link);


    }

    document.getElementById("npc").innerHTML = news;

  }
*/
