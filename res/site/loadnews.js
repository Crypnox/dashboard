function loadNews(newslink, newssite) {

  var loading = "<div class=\"spinner-border text-warning\" style=\"width: 4rem; height: 4rem;\" role=\"status\"></div>";
  document.getElementById(newssite).innerHTML = loading;

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      parseXML(this, newssite);
    }
  };
  xmlhttp.open("GET", newslink, true);
  xmlhttp.send();
}
