function parseXML(xml, newssite) {
  var itemXML = xml.responseXML.getElementsByTagName("item");
  //console.log(itemXML);

  var news = "";
  var title = "";
  var link = "";
  var description = "";
  var pubDate = "";

  for (var i = 0; i < itemXML.length; i++) {

    link = itemXML[i].getElementsByTagName("link")[0].childNodes[0].nodeValue.toString();
    description = itemXML[i].getElementsByTagName("description")[0].childNodes[0].nodeValue.toString();
    title = itemXML[i].getElementsByTagName("title")[0].childNodes[0].nodeValue.toString();
    pubDate = itemXML[i].getElementsByTagName("pubDate")[0].childNodes[0].nodeValue.toString();

    news += getHtml(title, description, link, pubDate);


  }

  document.getElementById(newssite).innerHTML = news;

}

function getHtml(title, description, link, pubDate) {

  return "<h7><a class=\"btn btn-dark\" target=\"_blank\" href=\"" + link + "\">Zum Artikel</a></h7>" + "<h4>" + title + "</h4>" + pubDate + "<br><br><br><h5>" + description + "</h5><hr>";
}
